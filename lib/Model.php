<?php 

namespace lib;

class Model extends Config
{
    protected $connection;

    public function __construct()
    {
        try {
            $this->connection = new \mysqli(self::DB_HOST, self::DB_USER, self::DB_PASSWORD, self::DB_DATABASE, self::DB_PORT);

            if ($this->connection->connect_error) {
                throw new \Exception('Error: ' . $this->connection->error . '<br />Error No: ' . $this->connection->errno);
            }
    
            $this->connection->set_charset("utf8");
            $this->connection->query("SET SQL_MODE = ''");
        } catch (\mysqli_sql_exception $e){
            throw new \Exception('Error: ' . $e);
        }
       
    }

    public function query($sql)
    {
        $query = $this->connection->query($sql);

        if (!$this->connection->errno) {
            if ($query instanceof \mysqli_result) {
                $data = array();

                while ($row = $query->fetch_assoc()) {
                    $data[] = $row;
                }

                $result = new \stdClass();
                $result->num_rows = $query->num_rows;
                $result->row = isset($data[0]) ? $data[0] : array();
                $result->rows = $data;

                $query->close();

                return $result;
            } else {
                return true;
            }
        } else {
            throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
        }
    }

    public function escape($value)
    {
        return $this->connection->real_escape_string($value);
    }
    
    public function countAffected()
    {
        return $this->connection->affected_rows;
    }

    public function getLastId()
    {
        return $this->connection->insert_id;
    }

    public function getTable($table)
    {
        $query = $this->query("SELECT * FROM " . $this->escape($table));

        return $query->rows;
    }

    public function deleteRow($table, $column, $value)
    {
        return $this->query("DELETE FROM " . $this->escape($table) . " WHERE " . $this->escape($column) . " = " . $this->escape($value));
    }

    public function insert($table, $data)
    {
        $sql = "INSERT INTO `" . $table . "` SET ";

        foreach ($data as $column => $value) {
            $sql .= "`" . $this->escape($column) . "` = '" . $this->escape($value) . "',";
        }

        $this->query(substr($sql, 0, -1));

        return $this->getLastId();
    }

    public function update($table, $data)
    {
        $sql = "UPDATE `" . $table['name'] . "` SET ";

        foreach ($data as $column => $value) {
            $sql .= "`" . $this->escape($column) . "` = '" . $this->escape($value) . "',";
        }

        $sql  = substr($sql, 0, -1);
        $sql .= "WHERE " . $this->escape($table['column_where']) . " = " . $this->escape($table['value_where']);

        return $this->query($sql);
    }

    public function getObjectByColumn($table, $column, $value)
    {
		$sql = "SELECT * FROM " . $this->escape($table) . " WHERE " . $this->escape($column) . " = '" . $this->escape($value) . "'";

		$query = $this->query($sql);

		return $query->row;
	}
}