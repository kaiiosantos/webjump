<?php
namespace lib;

class System extends Router
{
    private $url;
    private $exploder;
    private $area;
    private $controller;
    private $action;
    private $params;
    private $runController;

    public function __construct()
    {
        $this->setUrl();
        $this->setExploder();
        $this->setArea();
        $this->setController();
        $this->setAction();
        $this->setParams();
    }

    private function setUrl()
    {
        $this->url = isset($_GET['url']) ? $_GET['url']: header('Location: ' . APP_ROOT . 'dashboard/index');
    }

    private function setExploder()
    {
        $this->exploder = explode('/', $this->url);
    }

    private function setArea()
    {
        foreach ($this->routers as $indice => $router) {
            if ($this->isDefault && $this->exploder[0] == $indice) {
                $this->area = $router;
                $this->isDefault = false;
            }
        }

        $this->area = (!empty($this->area)) ? $this->area : $this->routerDefault;

        if (!defined('APP_AREA')) {
            define('APP_AREA', $this->area);
        }
    }

    public function getArea()
    {
        return $this->area;
    }

    private function setController()
    {
        $this->controller = ($this->isDefault) ? $this->exploder[0] :
            (empty($this->exploder[1]) || is_null($this->exploder[1]) || !isset($this->exploder[1]) ? 'dashboard' : $this->exploder[1]);
    }

    public function getController()
    {
        return $this->controller;
    }

    public function validateController()
    {
        if (!class_exists($this->runController)) {
            die('Controler not found: '. $this->runController); 
        }
    }

    private function setAction()
    {
        $this->action = $this->isDefault ?
            (!isset($this->exploder[1]) || is_null($this->exploder[1]) || empty($this->exploder[1]) ? 'index' : $this->exploder[1]) :
            (!isset($this->exploder[2]) || is_null($this->exploder[2]) || empty($this->exploder[2]) ? 'index' : $this->exploder[2]);
    }

    private function validateAction()
    {
        if (!(method_exists($this->runController, $this->action))) {
            die('Action not found: ' . $this->action);
        }
    }

    public function getAction()
    {
        return $this->action;
    }

    private function setParams()
    {
        if ($this->isDefault) {
            unset($this->exploder[0], $this->exploder[0]);
        } else {
            unset($this->exploder[0], $this->exploder[1], $this->exploder[2]);
        }

        if (end($this->exploder) == null) {
            array_pop($this->exploder);
        }

        if (empty($this->exploder)) {
            $this->params = array();
        } else {
            foreach ($this->exploder as $value) {
                $params[] = $value;
            }

            $this->params = $params;
        }
    }

    public function getParams($indice)
    {
        return isset($this->params[$indice]) ? $this->params[$indice] : null;
    }

    public function Run()
    {
        $this->runController = 'controller\\' . $this->area . '\\' . $this->controller;

        $this->validateController();
        $this->runController = new $this->runController();

        $this->validateAction();
        $act = $this->action;
        $this->runController->$act();
    }
}