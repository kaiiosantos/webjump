<?php
namespace lib;

class Controller extends System
{
    public $data = array();

    private $path;
    private $pathRender;

    public function __construct()
    {
        parent::__construct();
    }

    private function setPath($render)
    {
        if (is_array($render)) {
            foreach ($render as $li) {
                $path = 'view/' . $this->getArea() . '/' . $this->getController() . '/' . $li . '.phtml';
                $this->fileExists($path);
                $this->path[] = $path;
            }
        } else {
            $this->pathRender = is_null($render) ? $this->getAction() : $render;

            $this->path = 'view/' . $this->getArea() . '/' . $this->getController() . '/' . $this->pathRender . '.phtml';
            $this->fileExists($this->path);
        }
    }

    private function fileExists($file)
    {
        if (!file_exists($file)) {
            die('Não foi localizado o arquivo ' . $file);
        }
    }

    public function view($render = null)
    {
        $this->setPath($render);

        $this->render();
    }

    public function render($file = null)
    {
        if (is_array($this->data) && count($this->data) > 0) {
            extract($this->data);
        }

        if (!is_null($file) && is_array($file)) {
            foreach ($file as $li) {
                include($li);
            }
        } else if (is_null($file) && is_array($this->path)) {
            foreach ($this->path as $li) {
                include($li);
            }
        } else {
            $file = is_null($file) ? $this->path : $file;
            file_exists($file) ? include($file) : die($file);
        }
    }
}