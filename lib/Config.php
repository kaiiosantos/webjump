<?php 

namespace lib;

class Config{
    const DB_HOST     = 'localhost';
    const DB_USER     = 'root';
    const DB_PASSWORD = '';
    const DB_DATABASE = 'webjump';
    const DB_PORT     = '3306';

    const DB_CHARSET ='utf8';
}