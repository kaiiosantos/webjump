<?php

namespace controller\site;

use lib\Controller;
use model\site as model;

class Log extends Controller
{
    public function list()
    {
        $model_log = new model\Log;

        $all_log = $model_log->getTable('prd_log');

        $this->data['all_log'] = array();

        foreach ($all_log as $log) {
            $action = $log['key'];

            switch ($log['key']) {
                case 'add_category':
                    $action = 'Adição de categoria';
                    break;
                case 'edit_category':
                    $action = 'Alteração de categoria';
                    break;
                case 'delete_category':
                    $action = 'Exclusão de categoria';
                    break;
                case 'import_category':
                    $action = 'Importação de categorias';
                    break;
                case 'add_product':
                    $action = 'Adição de produção';
                    break;
                case 'edit_product':
                    $action = 'Alteração de produto';
                    break;
                case 'delete_product':
                    $action = 'Exclusão de produto';
                    break;
                case 'import_product':
                    $action = 'Importação de produtos';
                    break;
            }

            $this->data['all_log'][] = array(
                'action' => $action,
                'ip'     => $log['ip'],
                'date'   => date("d/m/Y H:i:s", strtotime($log['date_added']))
            );
        }

        $this->view('list');
    }
}