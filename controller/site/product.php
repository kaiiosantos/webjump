<?php

namespace controller\site;

use lib\Controller;
use model\site as model;

class Product extends Controller
{
    private $error = array();

    public function list()
    {
        $model_product  = new model\Product;

        $this->data['all_product']       = array();

        $all_product       = $model_product->getTable('prd_product');
        foreach ($all_product as $product) {
            $this->data['all_product'][] = array(
                'name'        => ucfirst(strtolower($product['name'])),
                'name_resume' => $this->resume(ucfirst(strtolower($product['name'])), 20),
                'sku'         => $product['sku'],
                'price'       => 'R$' . number_format($product['price'], 2, ',', '.'),
                'quantity'    => $product['quantity'],
                'categories'  => str_replace(',', ',<br/>', $model_product->getAllCategoryMatchProduct($product['id'])),
                'edit'        => APP_ROOT . 'product/edit&product_id=' . $product['id'],
                'delete'      => APP_ROOT . 'product/delete&product_id=' . $product['id']
            );
        }

        if (isset($_SESSION['success'])) {
            $this->data['success'] = $_SESSION['success'];
            unset($_SESSION['success']);
        }

        $this->data['url_add']    = APP_ROOT . 'product/add';
        $this->data['url_import'] = APP_ROOT . 'product/import';

        $this->view('list');
    }

    public function form()
    {
        $model_category = new model\Category;
        $model_product  = new model\Product;

        $this->data['all_category']      = $model_category->getTable('prd_category');

        $this->data['url_back']          = APP_ROOT . 'product/list';
        $this->data['url_submit']        = APP_ROOT . 'product/add';

        if (isset($_GET['product_id'])) {
            $product                 = $model_product->getProductById($_GET['product_id']);

            $this->data['url_submit'] = APP_ROOT . 'product/edit&product_id=' . $_GET['product_id'];
        } else {
            $this->data['url_submit'] = APP_ROOT . 'product/add';
        }

        $this->data['sku']               = $_POST['sku'] ?? $product['sku'] ?? '';
        $this->data['name']              = $_POST['name'] ?? $product['name'] ?? '';
        $this->data['price']             = $_POST['price'] ?? $product['price'] ?? '';
        $this->data['quantity']          = $_POST['quantity'] ?? $product['quantity'] ?? '';
        $this->data['description']       = $_POST['description'] ?? $product['description'] ?? '';

        if (isset($_POST['price'])) {
            $this->data['price'] = $_POST['price'];
        } else if (isset($product['price'])) {
            $this->data['price'] = number_format($product['price'], 2, ',', '.');
        } else {
            $this->data['price'] = '';
        }

        if (isset($_POST['categories'])) {
            $this->data['categories'] = $_POST['categories'];
        } else if (isset($product['categories'])) {
            $this->data['categories'] = explode(',', $product['categories']);
        } else {
            $this->data['categories'] = array();
        }

        if (isset($_POST['categories'])) {
            $this->data['categories'] = $_POST['categories'];
        } else if (isset($product['categories'])) {
            $this->data['categories'] = explode(',', $product['categories']);
        } else {
            $this->data['categories'] = array();
        }

        $this->data['error_sku']         = (isset($this->error['sku'])) ? $this->error['sku'] : '';
        $this->data['error_name']        = (isset($this->error['name'])) ? $this->error['name'] : '';
        $this->data['error_price']       = (isset($this->error['price'])) ? $this->error['price'] : '';
        $this->data['error_quantity']    = (isset($this->error['quantity'])) ? $this->error['quantity'] : '';
        $this->data['error_categories']  = (isset($this->error['categories'])) ? $this->error['categories'] : '';
        $this->data['error_description'] = (isset($this->error['description'])) ? $this->error['description'] : '';

        $this->view('form');
    }

    public function add()
    {
        $model_product = new model\Product;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') && ($this->validateForm($model_product))) {
            $model_product->query("SET AUTOCOMMIT=0");
            $model_product->query("START TRANSACTION");

            try {
                $data = array(
                    'sku'         => $_POST['sku'],
                    'name'        => $_POST['name'],
                    'price'       => $this->toFloat($_POST['price']),
                    'quantity'    => (float)$_POST['quantity'],
                    'description' => $_POST['description']
                );

                $product_id = $model_product->insert("prd_product", $data);

                foreach ($_POST['categories'] as $category_id) {
                    $data = array(
                        'product_id'  => $product_id,
                        'category_id' => $category_id
                    );

                    $model_product->insert("prd_product_category", $data);
                }

                $data = array(
                    'key'        => 'add_product',
                    'data'       => json_encode($_POST),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_product->insert("prd_log", $data);

                $model_product->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Produto adicionado";

                header('Location: ' . APP_ROOT . 'product/list');
            } catch (mysqli_sql_exception $e) {
                $model_product->query("ROLLBACK");
            }
        }

        $this->form();
    }

    public function edit()
    {
        $model_product = new model\Product;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') &&
            (isset($_GET['product_id'])) &&
            ($this->validateForm($model_product))) {

            $model_product->query("SET AUTOCOMMIT=0");
            $model_product->query("START TRANSACTION");

            try {
                $table = array(
                    'name'         => 'prd_product',
                    'column_where' => 'id',
                    'value_where'  => $_GET['product_id']
                );

                $data = array(
                    'sku'         => $_POST['sku'],
                    'name'        => $_POST['name'],
                    'price'       => $this->toFloat($_POST['price']),
                    'quantity'    => $_POST['quantity'],
                    'description' => $_POST['description']
                ); 

                $model_product->update($table, $data);

                $model_product->deleteRow('prd_product_category', 'product_id', $_GET['product_id']);

                foreach ($_POST['categories'] as $category_id) {
                    $data = array(
                        'product_id'  => $_GET['product_id'],
                        'category_id' => $category_id
                    );

                    $model_product->insert("prd_product_category", $data);
                }

                $data = array(
                    'key'        => 'edit_product',
                    'data'       => json_encode($_POST),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_product->insert("prd_log", $data);

                $model_product->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Produto editado";

                header('Location: ' . APP_ROOT . 'product/list');
            } catch (mysqli_sql_exception $e) {
                $model_product->query("ROLLBACK");
            }
        }

        $this->form();
    }

    public function delete()
    {
        $model_product = new model\Product;

        if (isset($_GET['product_id'])) {
            $model_product->query("SET AUTOCOMMIT=0");
            $model_product->query("START TRANSACTION");

            try {
                $model_product->deleteRow('prd_product', 'id', $_GET['product_id']);

                $data = array(
                    'key'        => 'delete_product',
                    'data'       => json_encode($_GET),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_product->insert("prd_log", $data);

                $model_product->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Produto deletado";

                header('Location: ' . APP_ROOT . 'product/list');
            } catch (mysqli_sql_exception $e) {
                $model_product->query("ROLLBACK");
            }
        }
    }

    public function import()
    {
        $model_product = new model\Product;
        $model_category = new model\Category;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') && ($this->validateImport())) {
            $model_product->query("SET AUTOCOMMIT=0");
            $model_product->query("START TRANSACTION");

            try {
                $count_categories = $this->importCategories($_FILES['file']['tmp_name'], $model_category);

                $count_products = $this->importProducts($_FILES['file']['tmp_name'], $model_product, $model_category);

                $data = array(
                    'key'        => 'import_product',
                    'data'       => json_encode(array(
                                                    'categories' => $count_categories,
                                                    'productus'  => $count_products)),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_product->insert("prd_log", $data);

                $model_product->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> " . $count_products . " produtos importados | " . $count_categories . " categorias importadas";

                header('Location: ' . APP_ROOT . 'product/list');
            } catch (mysqli_sql_exception $e) {
                $model_product->query("ROLLBACK");
            }
        }

        $this->data['url_back']          = APP_ROOT . 'product/list';
        $this->data['url_submit']        = APP_ROOT . 'product/import';

        $this->data['error_file']         = (isset($this->error['file'])) ? $this->error['file'] : '';

        $this->view('import');
    }

    private function importCategories($file, $model_category)
    {
        $count_import = 0;

        $file = fopen($_FILES['file']['tmp_name'], 'r');

        while(!feof($file)) {
            $row = fgets($file, 1024);

            $data = explode(';', $row);

            if ($data[0] != 'nome') {
                if (!empty($data[5])) {
                    $categories = explode('|', $data[5]);

                    foreach ($categories as $category) {
                        if ($this->validateImportCategory($model_category, $category)) {
                            $code = $model_category->getLastCode();

                            $data = array(
                                'code' => ++$code,
                                'name' => trim($category)
                            );

                            $model_category->insert('prd_category', $data);

                            $count_import++;
                        }
                    }
                }
            }
        }

        fclose($file);

        return $count_import;
    }

    private function importProducts($file, $model_product, $model_category)
    {
        $count_import = 0;

        $file = fopen($_FILES['file']['tmp_name'], 'r');

        while(!feof($file)) {
            $row = fgets($file, 1024);

            $data_file = explode(';', $row);

            if (($data_file[0] != 'nome') &&
                ($this->validateImportProduct($model_product, $data_file))) {
                $categories = array();
                //PEGANDO OS ID'S DAS CATEGORIAS
                if (!empty($data_file[5])) {
                    $all_categories = explode('|', $data_file[5]);

                    foreach ($all_categories as $category) {
                        $file_category = $model_category->getCategoryByName(trim($category));

                        if (!empty($file_category)) {
                            $categories[] = $file_category['id'];
                        }
                        
                    }
                }
                //ADD PRODUTO
                $data = array(
                    'name'        => trim($data_file[0]),
                    'sku'         => $data_file[1],
                    'description' => $data_file[2],
                    'quantity'    => $data_file[3],
                    'price'       => $data_file[4]
                );

                $product_id = $model_product->insert('prd_product', $data);
                if (count($categories) > 0) {
                    foreach ($categories as $category_id) {
                        $data = array(
                            'product_id'  => $product_id,
                            'category_id' => $category_id
                        );

                        $model_product->insert("prd_product_category", $data);
                    }
                }

                $count_import++;
            }
        }

        fclose($file);

        return $count_import;
    }

    private function validateForm($model_product = null)
    {
        if (strlen($_POST['sku']) <= 0 || strlen($_POST['sku']) > 13) {
            $this->error['sku'] = '<b>Falha:</b> Campo deve conter de 1 a 13 caracteres';
        }

        $productBySku = $model_product->getObjectByColumn('prd_product', 'sku', $_POST['sku']);

        if (!isset($_GET['product_id'])) {
            if ($productBySku) {
                $this->error['sku'] = '<b>Falha:</b> Código de barras já utilizado';
            }
        } else {
            if ($productBySku && ($_GET['product_id'] != $productBySku['id'])) {
                $this->error['sku'] = '<b>Falha:</b> Código de barras já utilizado';
            }
        }

        if (strlen($_POST['name']) <= 0 || strlen($_POST['name']) > 60) {
            $this->error['name'] = '<b>Falha:</b> Campo deve conter de 1 a 60 caracteres';
        }

        $productByName = $model_product->getObjectByColumn('prd_product', 'name', $_POST['name']);

        if (!isset($_GET['product_id'])) {
            if ($productByName) {
                $this->error['name'] = '<b>Falha:</b> Nome já utilizado';
            }
        } else {
            if ($productByName && ($_GET['product_id'] != $productByName['id'])) {
                $this->error['name'] = '<b>Falha:</b> Nome já utilizado';
            }
        }

        if (strlen($_POST['price']) <= 0) {
            $this->error['price'] = '<b>Falha:</b> Campo obrigatório';
        }

        if (strlen($_POST['quantity']) <= 0) {
            $this->error['quantity'] = '<b>Falha:</b> Campo obrigatório';
        }

        if (!isset($_POST['categories'])) {
            $this->error['categories'] = '<b>Falha:</b> Selecione pelo menos uma categoria';
        }

        if (strlen($_POST['description']) <= 0 || strlen($_POST['description']) > 100) {
            $this->error['description'] = '<b>Falha:</b> Campo deve conter de 1 a 100 caracteres';
        }

        return !$this->error;
    }

    private function validateImport()
    {
        if (strlen($_FILES['file']['tmp_name']) <= 0) {
            $this->error['file'] = '<b>Falha:</b> Campo obrigatório';
        }

        if (isset($_FILES['file']['tmp_name']) && !file_exists($_FILES['file']['tmp_name'])) {
            $this->error['file'] = '<b>Falha:</b> Arquivo não existe';
        }

        if( pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION) !== 'csv') {
            $this->error['file'] = '<b>Falha:</b> Arquivo inválido';
        }

        return !$this->error;
    }

    private function validateImportCategory($model_category, $category)
    {
        //SE O NOME ESTÁ VAZIO
        if (strlen(trim($category)) == 0) {
            return false;
        }
        //SE JÁ EXISTE A CATEGORIA
        $category_info = $model_category->getCategoryByName(trim($category));
        if (!empty($category_info)) {
            return false;
        }

        return true;
    }

    private function validateImportProduct($model_product, $product)
    {
        //SE JÁ EXISTE O PRODUTO
        $product_info = $model_product->getProductByName(trim($product[0]));
        if (!empty($product_info)) {
            return false;
        }
        //NOME VAZIO
        if (strlen($product[0]) == 0) {
            return false;
        }

        return true;
    }

    private function resume($var, $limite) {
        if (strlen($var) > $limite)	{
            $var = substr($var, 0, $limite);
            $var = trim($var) . "...";
        }
        return $var;
    }

    private function toFloat($num)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
       
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        } 
    
        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
}