<?php
namespace controller\site;

use lib\Controller;

class Dashboard extends Controller
{
    public function index()
    {
        $this->data['url_add_product'] = APP_ROOT . 'product/form'; //echo $view_teste; ;

        $this->view('dashboard');
    }
}