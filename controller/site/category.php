<?php

namespace controller\site;

use lib\Controller;
use model\site as model;

class Category extends Controller
{
    private $error = array();

    public function list()
    {
        $model = new model\Category;

        $all_category               = $model->getTable('prd_category');

        $this->data['all_category'] = array();

        foreach ($all_category as $category) {
            $this->data['all_category'][] = array(
                'id'     => $category['id'],
                'code'   => $category['code'],
                'name'   => ucfirst(strtolower($category['name'])),
                'edit'   => APP_ROOT . 'category/edit&category_id=' . $category['id'],
                'delete' => APP_ROOT . 'category/delete&category_id=' . $category['id']
            );
        }

        if (isset($_SESSION['success'])) {
            $this->data['success'] = $_SESSION['success'];
            unset($_SESSION['success']);
        }

        $this->data['url_add']    = APP_ROOT . 'category/add';
        $this->data['url_import'] = APP_ROOT . 'category/import';

        $this->view('list');
    }

    public function form()
    {
        $model = new model\Category;

        $this->data['url_back']       = APP_ROOT . 'category/list';

        if (isset($_GET['category_id'])) {
            $category                 = $model->getCategoryById($_GET['category_id']);

            $this->data['url_submit'] = APP_ROOT . 'category/edit&category_id=' . $_GET['category_id'];
        } else {
            $this->data['url_submit'] = APP_ROOT . 'category/add';
        }

        $this->data['code']           = $_POST['code'] ?? $category['code'] ?? '';
        $this->data['name']           = $_POST['name'] ?? $category['name'] ?? '';

        $this->data['error_code']     = (isset($this->error['code'])) ? $this->error['code'] : '';
        $this->data['error_name']     = (isset($this->error['name'])) ? $this->error['name'] : '';

        $this->view('form');
    }

    public function add()
    {
        $model_category = new model\Category;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') && ($this->validateForm($model_category))) {
            $model_category->query("SET AUTOCOMMIT=0");
            $model_category->query("START TRANSACTION");

            try {
                $model_category->insert("prd_category", $_POST);

                $data = array(
                    'key'        => 'add_category',
                    'data'       => json_encode($_POST),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_category->insert("prd_log", $data);
                
                $model_category->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Categoria adicionada";

                header('Location: ' . APP_ROOT . 'category/list');
            } catch (mysqli_sql_exception $e) {
                $model_category->query("ROLLBACK");
            }
        }

        $this->form();
    }

    public function edit()
    {
        $model_category = new model\Category;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') &&
            (isset($_GET['category_id'])) &&
            ($this->validateForm($model_category))) {
            $model_category->query("SET AUTOCOMMIT=0");
            $model_category->query("START TRANSACTION");

            try {
                $table = array(
                    'name'         => 'prd_category',
                    'column_where' => 'id',
                    'value_where'  => $_GET['category_id']
                );

                $model_category->update($table, $_POST);

                $data = array(
                    'key'        => 'edit_category',
                    'data'       => json_encode($_POST),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_category->insert("prd_log", $data);

                $model_category->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Categoria editada";

                header('Location: ' . APP_ROOT . 'category/list');
            } catch (mysqli_sql_exception $e) {
                $model_category->query("ROLLBACK");
            }
        }

        $this->form();
    }

    public function delete()
    {
        $model_category = new model\Category;

        if (isset($_GET['category_id'])) {
            $model_category->query("SET AUTOCOMMIT=0");
            $model_category->query("START TRANSACTION");

            try {
                $model_category->deleteRow('prd_category', 'id', $_GET['category_id']);

                $data = array(
                    'key'        => 'delete_category',
                    'data'       => json_encode($_GET),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_category->insert("prd_log", $data);

                $model_category->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> Categoria deletada";

                header('Location: ' . APP_ROOT . 'category/list');
            } catch (mysqli_sql_exception $e) {
                $model_category->query("ROLLBACK");
            }
        }
    }

    public function import()
    {
        $model_category = new model\Category;

        if (($_SERVER['REQUEST_METHOD'] == 'POST') && ($this->validateImport())) {
            $model_category->query("SET AUTOCOMMIT=0");
            $model_category->query("START TRANSACTION");

            try {
                $count_categories = $this->importCategories($_FILES['file']['tmp_name'], $model_category);

                $data = array(
                    'key'        => 'import_category',
                    'data'       => json_encode(array('categories' => $count_categories)),
                    'ip'         => $_SERVER['REMOTE_ADDR'],
                    'date_added' => date("Y-m-d H:i:s")
                );

                $model_category->insert("prd_log", $data);

                $model_category->query("COMMIT");

                $_SESSION['success'] = "<b>Sucesso:</b> " . $count_categories . " categorias importadas";

                header('Location: ' . APP_ROOT . 'category/list');
            } catch (mysqli_sql_exception $e) {
                $model_category->query("ROLLBACK");
            }
        }

        $this->data['url_back']          = APP_ROOT . 'category/list';
        $this->data['url_submit']        = APP_ROOT . 'category/import';

        $this->data['error_file']         = (isset($this->error['file'])) ? $this->error['file'] : '';

        $this->view('import');
    }

    private function importCategories($file, $model_category)
    {
        $count_import = 0;

        $file = fopen($_FILES['file']['tmp_name'], 'r');

        while(!feof($file)) {
            $row = fgets($file, 1024);

            $data = explode(';', $row);

            if ($data[0] != 'nome') {
                if (!empty($data[5])) {
                    $categories = explode('|', $data[5]);

                    foreach ($categories as $category) {
                        if ($this->validateImportCategory($model_category, $category)) {
                            $code = $model_category->getLastCode();

                            $data = array(
                                'code' => ++$code,
                                'name' => trim($category)
                            );

                            $model_category->insert('prd_category', $data);

                            $count_import++;
                        }
                    }
                }
            }
        }

        fclose($file);

        return $count_import;
    }

    private function validateImport()
    {
        if (strlen($_FILES['file']['tmp_name']) <= 0) {
            $this->error['file'] = '<b>Falha:</b> Campo obrigatório';
        }

        if (isset($_FILES['file']['tmp_name']) && !file_exists($_FILES['file']['tmp_name'])) {
            $this->error['file'] = '<b>Falha:</b> Arquivo não existe';
        }

        if( pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION) !== 'csv') {
            $this->error['file'] = '<b>Falha:</b> Arquivo inválido';
        }

        return !$this->error;
    }

    private function validateImportCategory($model_category, $category)
    {
        //SE O NOME ESTÁ VAZIO
        if (strlen(trim($category)) == 0) {
            return false;
        }
        //SE JÁ EXISTE A CATEGORIA
        $category_info = $model_category->getCategoryByName(trim($category));
        if (!empty($category_info)) {
            return false;
        }

        return true;
    }

    private function validateForm($model = null)
    {
        if (strlen($_POST['code']) <= 0 || strlen($_POST['code']) > 11) {
            $this->error['code'] = '<b>Falha:</b> Campo deve conter de 1 a 11 caracteres';
        }

        $categoryByCode = $model->getObjectByColumn('prd_category', 'code', $_POST['code']);

        if (!isset($_GET['category_id'])) {
            if ($categoryByCode) {
                $this->error['code'] = '<b>Falha:</b> Código já utilizado';
            }
        } else {
            if ($categoryByCode && ($_GET['category_id'] != $categoryByCode['id'])) {
                $this->error['code'] = '<b>Falha:</b> Código já utilizado';
            }
        }

        if (strlen($_POST['name']) <= 0 || strlen($_POST['name']) > 13) {
            $this->error['name'] = '<b>Falha:</b> Campo deve conter de 1 a 60 caracteres';
        }

        $categoryByName = $model->getObjectByColumn('prd_category', 'name', $_POST['name']);

        if (!isset($_GET['category_id'])) {
            if ($categoryByName) {
                $this->error['name'] = '<b>Falha:</b> Nome já utilizado';
            }
        } else {
            if ($categoryByName && ($_GET['category_id'] != $categoryByName['id'])) {
                $this->error['name'] = '<b>Falha:</b> Nome já utilizado';
            }
        }

        return !$this->error;
    }
}