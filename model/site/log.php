<?php
namespace model\site;

use lib\Model;

class Log extends Model{
  public function getCategoryById($category_id) {
    $sql = "SELECT
              c.id,
              c.code,
              c.name
            FROM prd_category c
            WHERE c.id = '" . $this->escape($category_id) . "'
    ";

    $query = $this->query($sql);

    return $query->row;
  }

  public function getCategoryByName($name) {
    $sql = "SELECT
              c.id,
              c.code,
              c.name
            FROM prd_category c
            WHERE c.name LIKE '" . $this->escape($name) . "'
    ";

    $query = $this->query($sql);

    return $query->row;
  }

  public function getCategoryByCode($code) {
    $sql = "SELECT
              c.id,
              c.code,
              c.name
            FROM prd_category c
            WHERE c.name LIKE '" . $this->escape($code) . "'
    ";

    $query = $this->query($sql);

    return $query->row;
  }

  public function getLastCode() {
    $sql = "SELECT
              MAX(c.code) AS code
            FROM prd_category c
    ";

    $query = $this->query($sql);

    return $query->row['code'];
  }
}
