<?php
namespace model\site;

use lib\Model;

class Product extends Model{
    public function getProductById($product_id) {
        $sql = "SELECT
                    p.id,
                    p.name,
                    p.sku,
                    p.price,
                    p.quantity,
                    p.description,
                    (SELECT group_concat(pc.category_id) as categories FROM prd_product_category pc
                    WHERE pc.product_id = '" . $this->escape($product_id) . "') as categories
                FROM prd_product p
                WHERE p.id = '" . $this->escape($product_id) . "'
        ";

        $query = $this->query($sql);

        return $query->row;
    }

    public function getProductByName($product_name) {
        $sql = "SELECT
                    p.id,
                    p.name,
                    p.sku,
                    p.price,
                    p.quantity,
                    p.description
                FROM prd_product p
                WHERE p.name LIKE '" . $this->escape($product_name) . "'
        ";

        $query = $this->query($sql);

        return $query->row;
    }

    public function getAllCategoryMatchProduct($product_id) {
      $sql = "SELECT
                group_concat(c.name) as categories
              FROM prd_product_category pc
              LEFT JOIN prd_category c ON c.id = pc.category_id
              WHERE pc.product_id = '" . $this->escape($product_id) . "'
      ";

      $query = $this->query($sql);

      return $query->row['categories'];
    }
}
