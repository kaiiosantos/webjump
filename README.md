## Desafio da Web Jump para desenvolvedor Backend
Link do desafio: https://bitbucket.org/webjump/assessment-backend/src/master/
	
Participante: Kaio Santos Rebouças - (75) 99140-4421 - kksantos18@gmail.com
	
Agradecimentos: Obrigado pela chance de realizar esse belo desafio, espero estar a altura do cargo e poder fazer parte dessa equipe.
	
---
	
## Como utilizar a aplicação web

1. Crie um banco de dados MySQL.
2. Utilize o script **webjump.sql** para criar a estrutura do banco de dados.
3. Após o banco criado, acesse o arquivo de configuração no caminho **lib/Config.php**.
4. Altere as constantes conforme os dados de acesso ao banco de dados criado.
5. Por último, acesse o caminho: **http://SERVIDOR/webjump/** / **Exemplo:** http://localhost/webjump/
6. Todas as funcionalidades poderão ser acessadas a partir do **menu** superior esquerdo.

---

## Explicação breve sobre a requisição

A URL é composta por **SERVER** / **CONTROLLER** / **ACTION**;
	
Ao acessar a url **‘http://SERVER/webjump/’**, a url será automaticamente alterada  para **‘http://SERVER/webjump/dashboard/index’**.
	
Isso porque o controller **‘dashboard’** e a função **‘index’** que está declarada dentro do controller, foram declarados como valores padrões da URL.

---

## Exemplo de requisição

**Acessando a lista de produtos direto pela URL:**

Se você tem um controller chamado **‘product’** e dentro do controller está declarado a função **‘list’**, se você quiser acessar a função list pela URL, basta acessar dessa forma **‘http://SERVIDOR/webjump/product/list’**

---

## Como funciona a codificação

1. É feito uma requisição.
2. As instruções do **‘.htaccess’** transforma o caminho da url em parâmetros **GET**
3. A partir desses parâmetros, a classe **System** é instanciada pelo index
4. A classe **System** é responsável por instanciar, recuperar e validar o controller e a ação passada pela url, fazendo suas respectivas chamadas.
5. Então o **controller**, realiza suas ações juntamente com **model** e no final é passado o resultado para **view**.
6. Na **view**, são exibidos todos os resultados para o usuário que as solicitou.

---

## Padrões e príncipios utilizados

1. PSR-1, PSR-2 e PSR-4.
2. SOLID.
3. MVC.
4. Changelog (GIT).

---

## Estrutura de arquivos

Segue uma breve explicação de cada diretório:

1. **Controller:** Arquivos responsáveis por intermediar a view e o model.
2. **Model:** Arquivos responsáveis por todas as interações com o banco de dados.
3. **View:** Responsável pela exibição dos dados.
4. **Lib:** Contém arquivos de configuração e classes gerais que serão herdadas.
5. **Assets:** Guarda todos os arquivos externos como CSS, JavaScript, Bibliotecas e Anexos;

---

## Saiba mais

1. Todos os requisitos, estruturas, padrões, versões, bibliotecas e tecnologias estão documentados no arquivo **Documentação.odt**.
2. Segue também o modelo de **ER** do banco de dados utilizado, feito através do MySQL Workbench.