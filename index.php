<?php 
define('VERSION', '1.0');
define('APP_ROOT', 'http://'. $_SERVER['HTTP_HOST'] . '/webjump/');

date_default_timezone_set('America/Bahia');

session_start();

require_once 'autoload.php';

use lib\System;

$System = new System();
$System->Run();